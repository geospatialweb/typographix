module.exports = {
  extends: [
    'stylelint-config-standard',
    'stylelint-config-css-modules',
    'stylelint-prettier/recommended'
  ],
  rules: {
    'at-rule-no-vendor-prefix': null,
    'property-no-vendor-prefix': null,
    'comment-empty-line-before': null,
    'selector-class-pattern': ['[a-z_]'],
    'selector-id-pattern': ['[a-z_]']
  }
}
