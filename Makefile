build:
	@npm run build

deploy:
	@npm run deploy

dev:
	@npm run dev

lint:
	@npm run lint

preview:
	@npm run preview

start:
	@npm start

stylelint:
	@npm run stylelint
