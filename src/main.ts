import 'reflect-metadata'
import { createPinia } from 'pinia'
import { createApp } from 'vue'

import { App } from '@/components'
import router from '@/router'
import '@/styles/scrollbar.css'
import '@/styles/screen.css'

createApp(App).use(createPinia()).use(router).mount('#app')
