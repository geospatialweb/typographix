import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import { Route } from '@/enums'
import { IRoute } from '@/interfaces'
import {
  About,
  Article_1,
  Article_2,
  Article_3,
  Article_4,
  Blog,
  Blog_Overview,
  Contact,
  Home,
  Page_Not_Found
} from '@/views'

const baseURL = import.meta.env.BASE_URL,
  history = createWebHistory(baseURL),
  route: IRoute = Route,
  routes: RouteRecordRaw[] = [
    {
      path: baseURL,
      redirect: `${baseURL}${route.HOME}}`
    },
    {
      path: `${baseURL}${route.HOME}`,
      name: route.HOME,
      component: Home
    },
    {
      path: `${baseURL}${route.ABOUT}`,
      name: route.ABOUT,
      component: About
    },
    {
      path: `${baseURL}${route.BLOG}`,
      name: route.BLOG,
      component: Blog,
      children: [
        {
          path: `${route.BLOG_OVERVIEW}`,
          name: route.BLOG_OVERVIEW,
          components: {
            blog: Blog_Overview
          }
        },
        {
          path: `${route.ARTICLE_1}`,
          name: route.ARTICLE_1,
          components: {
            blog: Article_1
          }
        },
        {
          path: `${route.ARTICLE_2}`,
          name: route.ARTICLE_2,
          components: {
            blog: Article_2
          }
        },
        {
          path: `${route.ARTICLE_3}`,
          name: route.ARTICLE_3,
          components: {
            blog: Article_3
          }
        },
        {
          path: `${route.ARTICLE_4}`,
          name: route.ARTICLE_4,
          components: {
            blog: Article_4
          }
        }
      ]
    },
    {
      path: `${baseURL}${route.CONTACT}`,
      name: route.CONTACT,
      component: Contact
    },
    {
      path: `${baseURL}:pathMatch(.*)*`,
      name: route.PAGE_NOT_FOUND,
      component: Page_Not_Found
    }
  ],
  router = createRouter({ history, routes })

export default router
