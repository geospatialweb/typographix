import { Container, Service } from 'typedi'

import { StoreState } from '@/enums'
import { IGallery } from '@/interfaces'
import { StoreService } from '@/services'

@Service()
export default class GalleryService {
  #storeService = Container.get(StoreService)

  #galleryStoreState = StoreState.GALLERY

  get galleryState() {
    return <IGallery>this.#storeService.getStoreState(this.#galleryStoreState)
  }

  set #galleryState(state: IGallery) {
    this.#storeService.setStoreState(this.#galleryStoreState, state)
  }

  displayEnlargedImage(src: string): void {
    this.#displayEnlargedImage(src)
  }

  removeEnlargedImage(): void {
    this.#removeEnlargedImage()
  }

  #displayEnlargedImage(src: string): void {
    const state: IGallery = { ...this.galleryState }
    if (!state.isActive) {
      state.isActive = !state.isActive
      state.src = src
      this.#setGalleryState(state)
    }
  }

  #removeEnlargedImage(): void {
    const state: IGallery = { ...this.galleryState }
    if (state.isActive) {
      state.isActive = !state.isActive
      state.src = ''
      this.#setGalleryState(state)
    }
  }

  #setGalleryState(state: IGallery): void {
    this.#galleryState = state
  }
}
