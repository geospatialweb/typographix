import { Container, Service } from 'typedi'

import { logo } from '@/config'
import { ElementId, StoreState } from '@/enums'
import { ILogo, IScrollPosition } from '@/interfaces'
import { StoreService } from '@/services'

@Service()
export default class ScrollService {
  #storeService = Container.get(StoreService)

  #document = document
  #logo: ILogo = { ...logo }
  #logoId = ElementId.LOGO
  #scrollPositionStoreState = StoreState.SCROLL_POSITION
  #window = window

  get #scrollPositionState() {
    return <IScrollPosition>this.#storeService.getStoreState(this.#scrollPositionStoreState)
  }

  set #scrollPositionState(position: IScrollPosition) {
    this.#storeService.setStoreState(this.#scrollPositionStoreState, position)
  }

  scrollToTop(): void {
    this.#window.scrollTo(0, 0)
  }

  setScrollPositionState(): void {
    this.#setScrollPositionState()
  }

  setLogoFontSize() {
    const logo = this.#document.getElementById(this.#logoId)!,
      adjustedFontSize = this.#adjustFontSize()
    logo.style.setProperty('font-size', `${adjustedFontSize.toString()}rem`)
  }

  #adjustFontSize(): number {
    const { position } = this.#scrollPositionState
    let adjustedFontSize = this.#logo.fontSizeDefault - position * this.#logo.adjustmentFactor
    adjustedFontSize = Math.max(this.#logo.fontSizeReduced, adjustedFontSize)
    adjustedFontSize = Math.min(this.#logo.fontSizeDefault, adjustedFontSize)
    return adjustedFontSize
  }

  #setScrollPositionState(): void {
    const position = window.scrollY
    this.#scrollPositionState = { position }
  }
}
