import cloneDeep from 'lodash.clonedeep'
import { defineStore } from 'pinia'
import { Service } from 'typedi'

import { contact, gallery, scrollPosition, theme } from '@/config'
import { IContactMessage, IGallery, IScrollPosition, IStoreState, ITheme } from '@/interfaces'
import { StoreState, UseStoreDefinition } from '@/types'

@Service()
export default class StoreService {
  #contact: IContactMessage = { ...contact }
  #gallery: IGallery = { ...gallery }
  #scrollPosition: IScrollPosition = { ...scrollPosition }
  #theme: ITheme = { ...theme }
  #useStore: UseStoreDefinition = defineStore('store', {})

  constructor() {
    this.#defineUseStore()
  }

  getStoreState(id: string): StoreState {
    return this.#useStore().getStoreState(id)
  }

  setStoreState(id: string, state: StoreState): void {
    this.#useStore().setStoreState(id, state)
  }

  #defineUseStore(): void {
    this.#useStore = defineStore('store', {
      state: (): IStoreState => ({
        contact: this.#contact,
        gallery: this.#gallery,
        scrollPosition: this.#scrollPosition,
        theme: this.#theme
      }),
      actions: {
        setStoreState(id: string, state: StoreState): void {
          this.$patch({ [id]: cloneDeep(state) })
        }
      },
      getters: {
        getStoreState: (state: IStoreState) => {
          return (id: string): StoreState => cloneDeep(state[id as keyof IStoreState])
        }
      }
    })
  }
}
