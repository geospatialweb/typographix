import { Container, Service } from 'typedi'

import { StoreState } from '@/enums'
import { IContactMessage } from '@/interfaces'
import { StoreService } from '@/services'

@Service()
export default class ContactService {
  #storeService = Container.get(StoreService)

  #contactStoreState = StoreState.CONTACT

  get contactMessageState() {
    return <IContactMessage>this.#storeService.getStoreState(this.#contactStoreState)
  }

  set #contactMessageState(message: IContactMessage) {
    this.#storeService.setStoreState(this.#contactStoreState, message)
  }

  submitContactMessage(message: string): void {
    this.#processContactMessage(message)
    this.#setContactMessageState()
    this.#setContactMessageStateTimeout()
  }

  #processContactMessage(message: string): void {
    console.log(message)
  }

  #setContactMessageState(): void {
    const state: IContactMessage = { ...this.contactMessageState }
    state.isActive = !state.isActive
    this.#contactMessageState = state
  }

  #setContactMessageStateTimeout(): void {
    window.setTimeout((): void => this.#setContactMessageState(), 2000)
  }
}
