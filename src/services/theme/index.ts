import { Container, Service } from 'typedi'

import { LocalStorageKey, RootElementAttribute, StoreState, ThemeMode } from '@/enums'
import { ITheme } from '@/interfaces'
import { StoreService } from '@/services'

@Service()
export default class ThemeService {
  #storeService = Container.get(StoreService)

  #dataThemeAttribute = RootElementAttribute.DATA_THEME
  #darkMode = ThemeMode.DARK
  #lightMode = ThemeMode.LIGHT
  #localStorage = localStorage
  #localStorageKey = LocalStorageKey.TYPOGRAPHIX
  #rootElement = document.documentElement
  #themeStoreState = StoreState.THEME

  get themeState() {
    return <ITheme>this.#storeService.getStoreState(this.#themeStoreState)
  }

  set #themeState(theme: ITheme) {
    this.#storeService.setStoreState(this.#themeStoreState, theme)
  }

  setInitialTheme(): void {
    const currentThemeFromLocalStorage = this.#localStorage.getItem(this.#localStorageKey)
    if (!currentThemeFromLocalStorage) {
      this.#localStorage.setItem(this.#localStorageKey, this.#lightMode)
      this.#rootElement.setAttribute(this.#dataThemeAttribute, this.#lightMode)
    } else {
      const { theme: currentThemeFromStore } = this.themeState
      if (currentThemeFromLocalStorage !== currentThemeFromStore) {
        this.#rootElement.setAttribute(this.#dataThemeAttribute, currentThemeFromLocalStorage)
        this.#themeState = { theme: currentThemeFromLocalStorage }
      }
    }
  }

  toggleTheme(theme: string): void {
    this.#setRootElement(theme)
    this.#setLocalStorage(theme)
    this.#setThemeState(theme)
  }

  #setRootElement(theme: string): void {
    theme === <string>this.#lightMode
      ? this.#rootElement.setAttribute(this.#dataThemeAttribute, this.#darkMode)
      : this.#rootElement.setAttribute(this.#dataThemeAttribute, this.#lightMode)
  }

  #setLocalStorage(theme: string): void {
    theme === <string>this.#lightMode
      ? this.#localStorage.setItem(this.#localStorageKey, this.#darkMode)
      : this.#localStorage.setItem(this.#localStorageKey, this.#lightMode)
  }

  #setThemeState(theme: string): void {
    if (theme === <string>this.#lightMode) {
      this.#themeState = { theme: this.#darkMode }
    }
    if (theme === <string>this.#darkMode) {
      this.#themeState = { theme: this.#lightMode }
    }
  }
}
