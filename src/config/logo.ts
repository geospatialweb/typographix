export default {
  adjustmentFactor: 0.03,
  fontSizeDefault: 4.5,
  fontSizeReduced: 3
}
