import 'vue/jsx'
import { defineComponent } from 'vue'

import { Logo, Navigation, Theme_Switcher } from '@/components'
import styles from './index.module.css'

export default defineComponent({
  name: 'Header Component',
  setup() {
    const { header } = styles
    return (): JSX.Element => (
      <header class={header}>
        <Logo />
        <Navigation />
        <Theme_Switcher />
      </header>
    )
  }
})
