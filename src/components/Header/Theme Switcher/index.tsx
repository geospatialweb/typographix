import 'vue/jsx'
import { Container } from 'typedi'
import { defineComponent } from 'vue'

import { ThemeMode } from '@/enums'
import { ITheme } from '@/interfaces'
import { ThemeService } from '@/services'
import styles from './index.module.css'

export default defineComponent({
  name: 'Theme Switcher Component',
  setup() {
    const { fas, theme_switcher } = styles,
      lightThemeMode: string = ThemeMode.LIGHT,
      themeService = Container.get(ThemeService),
      getThemeState = (): ITheme => {
        const { themeState } = themeService
        return themeState
      },
      onClickHandler = (theme: string): void => themeService.toggleTheme(theme),
      jsx = ({ theme }: ITheme): JSX.Element => (
        <div class={theme_switcher} onClick={(): void => onClickHandler(theme)}>
          {theme === lightThemeMode ? (
            <div aria-label="light mode">
              <span>Light Mode</span>
              <i class={`${fas} fas fa-sun`}></i>
            </div>
          ) : (
            <div aria-label="dark mode">
              <span>Dark Mode</span>
              <i class={`${fas} fas fa-moon`}></i>
            </div>
          )}
        </div>
      )
    return (): JSX.Element => jsx(getThemeState())
  }
})
