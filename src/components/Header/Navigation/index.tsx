import 'vue/jsx'
import { Container } from 'typedi'
import { defineComponent } from 'vue'

import { Route } from '@/enums'
import { IRoute } from '@/interfaces'
import { RouterService, ScrollService } from '@/services'
import styles from './index.module.css'

export default defineComponent({
  name: 'Navigation Component',
  setup() {
    const { nav } = styles,
      route: IRoute = Route,
      scrollToTop = (): void => {
        const scrollService = Container.get(ScrollService)
        scrollService.scrollToTop()
      },
      setRoute = async (route: string): Promise<void> => {
        const routerService = Container.get(RouterService)
        await routerService.setRoute(route)
      },
      onClickHandler = (route: string): void => {
        void setRoute(route)
        scrollToTop()
      }
    return (): JSX.Element => (
      <nav class={nav} aria-label="navigation">
        <a onClick={(): void => onClickHandler(route.HOME)}>Home</a>
        <a onClick={(): void => onClickHandler(route.ABOUT)}>About</a>
        <a onClick={(): void => onClickHandler(route.BLOG_OVERVIEW)}>Blog</a>
        <a onClick={(): void => onClickHandler(route.CONTACT)}>Contact</a>
      </nav>
    )
  }
})
