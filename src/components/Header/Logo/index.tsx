import 'vue/jsx'
import { defineComponent } from 'vue'

import styles from './index.module.css'

export default defineComponent({
  name: 'Logo Component',
  setup() {
    const { logo } = styles
    return (): JSX.Element => (
      <div class={logo}>
        <h1 id="logo" aria-label="typographix logo">
          Typographix
        </h1>
      </div>
    )
  }
})
