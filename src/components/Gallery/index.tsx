import 'vue/jsx'
import { Container } from 'typedi'
import { defineComponent } from 'vue'

import { GalleryService } from '@/services'
import styles from './index.module.css'

export default defineComponent({
  name: 'Gallery Component',
  setup() {
    const { gallery, image, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 } = styles,
      /* eslint-disable */
      displayEnlargedImage = (evt: any): void => {
        /* prettier-ignore */
        const { target: { src } } = evt,
          galleryService = Container.get(GalleryService)
        galleryService.displayEnlargedImage(src)
      },
      /* eslint-enable */
      onClickHandler = (evt: MouseEvent): void => displayEnlargedImage(evt)
    return (): JSX.Element => (
      <div class={gallery}>
        <div class={`${image} ${image1}`}>
          <img
            src="https://source.unsplash.com/random?abstract-1"
            onClick={(evt): void => onClickHandler(evt)}
            title="Click to Enlarge"
            alt="Random Abstract Image"
          />
        </div>
        <div class={`${image} ${image2}`}>
          <img
            src="https://source.unsplash.com/random?abstract-2"
            onClick={(evt): void => onClickHandler(evt)}
            title="Click to Enlarge"
            alt="Random Abstract Image"
          />
        </div>
        <div class={`${image} ${image3}`}>
          <img
            src="https://source.unsplash.com/random?abstract-3"
            onClick={(evt): void => onClickHandler(evt)}
            title="Click to Enlarge"
            alt="Random Abstract Image"
          />
        </div>
        <div class={`${image} ${image4}`}>
          <img
            src="https://source.unsplash.com/random?abstract-4"
            onClick={(evt): void => onClickHandler(evt)}
            title="Click to Enlarge"
            alt="Random Abstract Image"
          />
        </div>
        <div class={`${image} ${image5}`}>
          <img
            src="https://source.unsplash.com/random?abstract-5"
            onClick={(evt): void => onClickHandler(evt)}
            title="Click to Enlarge"
            alt="Random Abstract Image"
          />
        </div>
        <div class={`${image} ${image6}`}>
          <img
            src="https://source.unsplash.com/random?abstract-6"
            onClick={(evt): void => onClickHandler(evt)}
            title="Click to Enlarge"
            alt="Random Abstract Image"
          />
        </div>
        <div class={`${image} ${image7}`}>
          <img
            src="https://source.unsplash.com/random?abstract-7"
            onClick={(evt): void => onClickHandler(evt)}
            title="Click to Enlarge"
            alt="Random Abstract Image"
          />
        </div>
        <div class={`${image} ${image8}`}>
          <img
            src="https://source.unsplash.com/random?abstract-8"
            onClick={(evt): void => onClickHandler(evt)}
            title="Click to Enlarge"
            alt="Random Abstract Image"
          />
        </div>
        <div class={`${image} ${image9}`}>
          <img
            src="https://source.unsplash.com/random?abstract-9"
            onClick={(evt): void => onClickHandler(evt)}
            title="Click to Enlarge"
            alt="Random Abstract Image"
          />
        </div>
        <div class={`${image} ${image10}`}>
          <img
            src="https://source.unsplash.com/random?abstract-10"
            onClick={(evt): void => onClickHandler(evt)}
            title="Click to Enlarge"
            alt="Random Abstract Image"
          />
        </div>
      </div>
    )
  }
})
