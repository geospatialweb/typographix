import 'vue/jsx'
import { Container } from 'typedi'
import { defineComponent } from 'vue'

import { Route } from '@/enums'
import { IRoute } from '@/interfaces'
import { RouterService, ScrollService } from '@/services'
import styles from './index.module.css'

export default defineComponent({
  name: 'Blog Overview Component',
  setup() {
    const { blog, feature_article, sub_articles, sub_article } = styles,
      route: IRoute = Route,
      scrollToTop = (): void => {
        const scrollService = Container.get(ScrollService)
        scrollService.scrollToTop()
      },
      setRoute = async (route: string): Promise<void> => {
        const routerService = Container.get(RouterService)
        await routerService.setRoute(route)
      },
      onClickHandler = (route: string): void => {
        void setRoute(route)
        scrollToTop()
      }
    return (): JSX.Element => (
      <div class={blog}>
        <div class={feature_article}>
          <img src="/assets/images/computer_setup.webp" alt="Computer Setup" />
          <h1>Enhancing Productivity: Revolutionizing Modern Workspaces</h1>
          <p>
            In the rapidly evolving landscape of work, the concept of productivity has taken center stage, prompting a
            radical transformation in the design and functionality of modern workspaces. Gone are the days of rigid
            cubicles and monotonous routines...
          </p>
          <a onClick={(): void => onClickHandler(route.ARTICLE_1)}>Read More</a>
        </div>
        <div class={sub_articles}>
          <div class={sub_article}>
            <img src="/assets/images/robot_on_bench.webp" alt="Robot on Bench" />
            <h2>Unveiling the Future: Quantum Leaps in Humanoid Robotics</h2>
            <p>
              In a startling turn of events, the field of robotics has taken a monumental leap forward with the
              introduction of cutting-edge humanoid robots that blur the line between science fiction and reality...
            </p>
            <a onClick={(): void => onClickHandler(route.ARTICLE_2)}>Read More</a>
          </div>
          <div class={sub_article}>
            <img src="/assets/images/underwater_computer.webp" alt="Underwater Computer" />
            <h2>Diving Deep: Underwater Cooling Computer Hardware</h2>
            <p>
              In the realm of cutting-edge technology, a groundbreaking solution has emerged to tackle the persistent
              challenge of cooling high-performance computer hardware: underwater cooling...
            </p>
            <a onClick={(): void => onClickHandler(route.ARTICLE_3)}>Read More</a>
          </div>
          <div class={sub_article}>
            <img src="/assets/images/windmill_farm.webp" alt="Windmill Farm" />
            <h2>Harnessing the Breeze: The Future of Wind Energy</h2>
            <p>
              In a world where sustainable energy sources are gaining paramount importance, wind energy emerges as a
              frontrunner in the race to combat climate change and ensure a greener future...
            </p>
            <a onClick={(): void => onClickHandler(route.ARTICLE_4)}>Read More</a>
          </div>
        </div>
      </div>
    )
  }
})
