import 'vue/jsx'
import { defineComponent } from 'vue'

export default defineComponent({
  name: 'Blog Component',
  setup() {
    return (): JSX.Element => <router-view name="blog" />
  }
})
