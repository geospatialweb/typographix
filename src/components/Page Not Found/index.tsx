import 'vue/jsx'
import { Container } from 'typedi'
import { defineComponent, onMounted, onUnmounted } from 'vue'

import { Route } from '@/enums'
import { IRoute } from '@/interfaces'
import { RouterService } from '@/services'
import styles from './index.module.css'

export default defineComponent({
  name: 'Page Not Found Component',
  setup() {
    const { page_not_found } = styles,
      routeTimeout = window.setTimeout(async (): Promise<void> => await setRoute(), 2000),
      setRoute = async (): Promise<void> => {
        const route: IRoute = Route,
          routerService = Container.get(RouterService)
        await routerService.setRoute(route.HOME)
      }
    onMounted((): number => routeTimeout)
    onUnmounted((): void => window.clearTimeout(routeTimeout))
    return (): JSX.Element => (
      <div class={page_not_found}>
        <img src="/assets/images/404.webp" alt="404 Page Not Found" />
      </div>
    )
  }
})
