import 'vue/jsx'
import { Container } from 'typedi'
import { defineComponent } from 'vue'

import { IGallery } from '@/interfaces'
import { GalleryService } from '@/services'
import styles from './index.module.css'

export default defineComponent({
  name: 'Gallery Enlarged Component',
  setup() {
    const { active, inactive } = styles,
      galleryService = Container.get(GalleryService),
      getGalleryState = (): IGallery => {
        const { galleryState } = galleryService
        return galleryState
      },
      onClickHandler = (): void => galleryService.removeEnlargedImage(),
      jsx = ({ isActive, src }: IGallery): JSX.Element => (
        <img
          class={isActive ? active : inactive}
          src={src}
          onClick={(): void => onClickHandler()}
          title="Click to Remove"
          alt="Enlarged Random Abstract Image"
        />
      )
    return (): JSX.Element => jsx(getGalleryState())
  }
})
