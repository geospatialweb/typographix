import 'vue/jsx'
import { Container } from 'typedi'
import { defineComponent, onMounted } from 'vue'

import { Header } from '@/components'
import { ScrollService, ThemeService } from '@/services'
import styles from './index.module.css'

export default defineComponent({
  name: 'App Component',
  setup() {
    const { main } = styles,
      setInitialTheme = (): void => {
        const themeService = Container.get(ThemeService)
        themeService.setInitialTheme()
      },
      onScrollHandler = (): void => {
        const scrollService = Container.get(ScrollService)
        scrollService.setScrollPositionState()
        scrollService.setLogoFontSize()
      }
    onMounted((): void => {
      setInitialTheme()
      window.addEventListener('scroll', onScrollHandler, { passive: true })
    })
    return (): JSX.Element => (
      <>
        <Header />
        <main class={main}>
          <router-view />
        </main>
      </>
    )
  }
})
