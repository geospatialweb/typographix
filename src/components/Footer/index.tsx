import 'vue/jsx'
import { defineComponent } from 'vue'

import styles from './index.module.css'

export default defineComponent({
  name: 'Footer Component',
  setup() {
    const { footer, social_media_icons, spacer } = styles
    return (): JSX.Element => (
      <footer class={footer} aria-label="footer">
        <small aria-label="copyright 2024 typographix">&copy; 2024 Typographix</small>
        <div class={social_media_icons}>
          <a href="#" aria-label="facebook">
            <i class={`${spacer} fa-brands fa-facebook`} title="Facebook"></i>
          </a>
          <a href="#" aria-label="x-twitter">
            <i class={`${spacer} fa-brands fa-x-twitter`} title="Twitter"></i>
          </a>
          <a href="#" aria-label="instagram">
            <i class={`${spacer} fa-brands fa-instagram`} title="Instagram"></i>
          </a>
          <a href="#" aria-label="linkedin">
            <i class={`${spacer} fa-brands fa-linkedin-in`} title="LinkedIn"></i>
          </a>
        </div>
      </footer>
    )
  }
})
