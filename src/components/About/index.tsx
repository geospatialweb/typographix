import 'vue/jsx'
import { defineComponent } from 'vue'

import styles from './index.module.css'

export default defineComponent({
  name: 'About Component',
  setup() {
    const { about, box1, box2, box3, box4, box5, box6, grid, iconbox, textbox } = styles
    return (): JSX.Element => (
      <div class={about}>
        <h1>About</h1>
        <div class={grid}>
          <div class={box1}>
            <div class={textbox}>
              <h2>Typographix</h2>
              <p>
                Lorem ipsum dolor sit amet consectetur vero adipisicing elit. Inventore voluptates obcaecati impedit
                rerum amet. Exercitationem, sit? Nobis facilis modi reprehenderit amet aspernatur explicabo dolor
                perferendis voluptates dolore in dicta? Delectus, necessitatibus? Lorem ipsum dolor, sit amet
                consectetur facilis ratione adipisicing elit. Possimus dolor excepturi aspernatur dolorem accusantium
                aliquam, itaque vero ratione omnis, facilis, quo neque doloribus consectetur.
              </p>
            </div>
          </div>
          <div class={`${iconbox} ${box2}`}>
            <i class="fa-solid fa-code"></i>
            <h2>Quality Code</h2>
            <p>Lorem ipsum dolor sit amet consectetur. Nobis vel fugiat temporibus.</p>
          </div>
          <div class={`${iconbox} ${box3}`}>
            <i class="fa-solid fa-wand-magic-sparkles"></i>
            <h2>CSS Wizard</h2>
            <p>Lorem ipsum dolor sit amet consectetur. Nobis vel fugiat temporibus.</p>
          </div>
          <div class={`${iconbox} ${box4}`}>
            <i class="fa-solid fa-trophy"></i>
            <h2>Award Winning</h2>
            <p>Lorem ipsum dolor sit amet consectetur. Nobis vel fugiat temporibus.</p>
          </div>
          <div class={`${iconbox} ${box5} `}>
            <i class="fa-solid fa-infinity"></i>
            <h2>Infinite Ideas</h2>
            <p>Lorem ipsum dolor sit amet consectetur. Nobis vel fugiat temporibus.</p>
          </div>
          <div class={box6}>
            <div class={textbox}>
              <h2>Typographix Team</h2>
              <p>
                Lorem ipsum dolor sit delectus aspernatur consectetur adipisicing elit. Inventore voluptates obcaecati
                voluptate rerum amet. Exercitationem, sit? Nobis facilis modi reprehenderit amet aspernatur explicabo
                dolor perferendis dolore in dicta? Delectus, necessitatibus? Lorem ipsum, dolor sit amet consectetur
                adipisicing elit. Corrupti ipsam est delectus labore porro voluptate dolores. Est possimus aperiam illo
                maxime ad alias omnis? Qui laborum corrupti incidunt possimus ratione!
              </p>
            </div>
          </div>
        </div>
      </div>
    )
  }
})
