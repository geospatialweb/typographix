import 'vue/jsx'
import { Container } from 'typedi'
import { defineComponent } from 'vue'

import { IContactMessage } from '@/interfaces'
import { ContactService } from '@/services'
import styles from './index.module.css'

export default defineComponent({
  name: 'Contact Component',
  setup() {
    const { active, button, contact, inactive, form } = styles,
      contactService = Container.get(ContactService),
      getContactMessageState = (): IContactMessage => {
        const { contactMessageState } = contactService
        return contactMessageState
      },
      submitContactMessage = (): void => {
        const textarea = document.getElementById('textarea') as HTMLTextAreaElement,
          message = textarea.value.trim()
        if (message != '') {
          contactService.submitContactMessage(message)
          textarea.value = ''
        }
      },
      onClickHandler = (): void => submitContactMessage(),
      jsx = ({ isActive }: IContactMessage): JSX.Element => (
        <div class={contact} aria-label="contact form">
          <h1>
            <a href="mailto:example@example.com">Contact Us</a>
          </h1>
          <form class={form}>
            <fieldset>
              <textarea id="textarea"></textarea>
              <p class={`${isActive ? active : inactive}`}>Message Submitted!</p>
            </fieldset>
          </form>
          <div class={button}>
            <button onClick={(): void => onClickHandler()}>Submit</button>
          </div>
        </div>
      )
    return (): JSX.Element => jsx(getContactMessageState())
  }
})
