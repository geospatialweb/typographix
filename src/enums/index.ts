export enum DocumentTitle {
  ABOUT = 'Typographix About',
  ARTICLE_1 = 'Enhancing Productivity: Revolutionizing Modern Workspaces',
  ARTICLE_2 = 'Unveiling the Future: Quantum Leaps in Humanoid Robotics',
  ARTICLE_3 = 'Diving Deep: Underwater Cooling Computer Hardware',
  ARTICLE_4 = 'Harnessing the Breeze: The Future of Wind Energy',
  BLOG = 'Typographix Blog',
  CONTACT = 'Typographix Contact',
  HOME = 'Typographix Home',
  PAGE_NOT_FOUND = 'Typographix Page Not Found'
}

export enum ElementId {
  LOGO = 'logo'
}

export enum LocalStorageKey {
  TYPOGRAPHIX = 'typographix'
}

export enum RootElementAttribute {
  DATA_THEME = 'data-theme'
}

export enum Route {
  ABOUT = 'about',
  ARTICLE_1 = 'article_1',
  ARTICLE_2 = 'article_2',
  ARTICLE_3 = 'article_3',
  ARTICLE_4 = 'article_4',
  BLOG = 'blog',
  BLOG_OVERVIEW = 'blog_overview',
  CONTACT = 'contact',
  HOME = 'home',
  PAGE_NOT_FOUND = 'page_not_found'
}

export enum StoreState {
  CONTACT = 'contact',
  GALLERY = 'gallery',
  SCROLL_POSITION = 'scrollPosition',
  THEME = 'theme'
}

export enum ThemeMode {
  DARK = 'dark',
  LIGHT = 'light'
}
