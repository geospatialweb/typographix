import 'vue/jsx'
import { defineComponent, onMounted } from 'vue'

import { About } from '@/components'
import { DocumentTitle } from '@/enums'

export default defineComponent({
  name: 'About View',
  setup() {
    const about: string = DocumentTitle.ABOUT
    onMounted((): string => (document.title = about))
    return (): JSX.Element => (
      <section>
        <About />
      </section>
    )
  }
})
