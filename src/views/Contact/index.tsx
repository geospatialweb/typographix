import 'vue/jsx'
import { defineComponent, onMounted } from 'vue'

import { Contact } from '@/components'
import { DocumentTitle } from '@/enums'

export default defineComponent({
  name: 'Contact View',
  setup() {
    const contact: string = DocumentTitle.CONTACT
    onMounted((): string => (document.title = contact))
    return (): JSX.Element => (
      <section>
        <Contact />
      </section>
    )
  }
})
