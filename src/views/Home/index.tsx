import 'vue/jsx'
import { defineComponent, onMounted } from 'vue'

import { Footer, Gallery, Gallery_Enlarged } from '@/components'
import { DocumentTitle } from '@/enums'

export default defineComponent({
  name: 'Home View',
  setup() {
    const home: string = DocumentTitle.HOME
    onMounted((): string => (document.title = home))
    return (): JSX.Element => (
      <section>
        <Gallery />
        <Gallery_Enlarged />
        <Footer />
      </section>
    )
  }
})
