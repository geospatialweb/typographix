import 'vue/jsx'
import { defineComponent, onMounted } from 'vue'

import { Article_3 } from '@/components'
import { DocumentTitle } from '@/enums'

export default defineComponent({
  name: 'Article 3 View',
  setup() {
    const blog: string = DocumentTitle.BLOG,
      article_3: string = DocumentTitle.ARTICLE_3
    onMounted((): string => (document.title = `${blog} - ${article_3}`))
    return (): JSX.Element => <Article_3 />
  }
})
