import 'vue/jsx'
import { defineComponent, onMounted } from 'vue'

import { Article_2 } from '@/components'
import { DocumentTitle } from '@/enums'

export default defineComponent({
  name: 'Article 2 View',
  setup() {
    const blog: string = DocumentTitle.BLOG,
      article_2: string = DocumentTitle.ARTICLE_2
    onMounted((): string => (document.title = `${blog} - ${article_2}`))
    return (): JSX.Element => <Article_2 />
  }
})
