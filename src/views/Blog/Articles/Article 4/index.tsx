import 'vue/jsx'
import { defineComponent, onMounted } from 'vue'

import { Article_4 } from '@/components'
import { DocumentTitle } from '@/enums'

export default defineComponent({
  name: 'Article 4 View',
  setup() {
    const blog: string = DocumentTitle.BLOG,
      article_4: string = DocumentTitle.ARTICLE_4
    onMounted((): string => (document.title = `${blog} - ${article_4}`))
    return (): JSX.Element => <Article_4 />
  }
})
