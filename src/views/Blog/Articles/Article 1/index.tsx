import 'vue/jsx'
import { defineComponent, onMounted } from 'vue'

import { Article_1 } from '@/components'
import { DocumentTitle } from '@/enums'

export default defineComponent({
  name: 'Article 1 View',
  setup() {
    const blog: string = DocumentTitle.BLOG,
      article_1: string = DocumentTitle.ARTICLE_1
    onMounted((): string => (document.title = `${blog} - ${article_1}`))
    return (): JSX.Element => <Article_1 />
  }
})
