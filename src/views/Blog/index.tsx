import 'vue/jsx'
import { defineComponent, onMounted } from 'vue'

import { Blog } from '@/components'
import { DocumentTitle } from '@/enums'

export default defineComponent({
  name: 'Blog View',
  setup() {
    const blog: string = DocumentTitle.BLOG
    onMounted((): string => (document.title = blog))
    return (): JSX.Element => (
      <section>
        <Blog />
      </section>
    )
  }
})
