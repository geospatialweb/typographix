import 'vue/jsx'
import { defineComponent } from 'vue'

import { Blog_Overview } from '@/components'

export default defineComponent({
  name: 'Blog Overview View',
  setup() {
    return (): JSX.Element => <Blog_Overview />
  }
})
