import 'vue/jsx'
import { defineComponent, onMounted } from 'vue'

import { Page_Not_Found } from '@/components'
import { DocumentTitle } from '@/enums'

export default defineComponent({
  name: 'Page Not Found View',
  setup() {
    const pageNotFound: string = DocumentTitle.PAGE_NOT_FOUND
    onMounted((): string => (document.title = pageNotFound))
    return (): JSX.Element => (
      <section>
        <Page_Not_Found />
      </section>
    )
  }
})
