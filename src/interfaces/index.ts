export interface IContactMessage {
  isActive: boolean
}

export interface IGallery {
  isActive: boolean
  src: string
}

export interface ILogo {
  adjustmentFactor: number
  fontSizeDefault: number
  fontSizeReduced: number
}

export interface IRoute {
  ABOUT: string
  ARTICLE_1: string
  ARTICLE_2: string
  ARTICLE_3: string
  ARTICLE_4: string
  BLOG: string
  BLOG_OVERVIEW: string
  CONTACT: string
  HOME: string
  PAGE_NOT_FOUND: string
}

export interface IScrollPosition {
  position: number
}

export interface IStoreState {
  contact: IContactMessage
  gallery: IGallery
  scrollPosition: IScrollPosition
  theme: ITheme
}

export interface ITheme {
  theme: string
}
