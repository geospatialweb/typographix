import { StoreDefinition } from 'pinia'

import { IContactMessage, IGallery, IScrollPosition, IStoreState, ITheme } from '@/interfaces'

export type StoreState = IContactMessage | IGallery | IScrollPosition | ITheme

export type UseStoreDefinition = StoreDefinition<
  'store',
  IStoreState,
  { getStoreState: (state: IStoreState) => (id: string) => StoreState },
  { setStoreState(id: string, state: StoreState): void }
>
